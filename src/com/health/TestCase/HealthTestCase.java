package com.health.TestCase;

import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.health.entity.HealthMainClass;
import com.health.entity.User;

public class HealthTestCase {
	
	
	@BeforeClass
	  public static void testSetup() {
	  }
	
	@AfterClass
	  public static void testCleanup() {
	    // Do your cleanup here like close URL connection , releasing resources etc
	  }
	
	@Test
	public void createPremiumForUser() {
		
		User user = new User();
		user.setName("Norman Gomes");
		user.setAge(34);
		user.setGender("Male");
		
		ArrayList<String> habbits = new ArrayList<String>();
		habbits.add("Daily exercise");
		habbits.add("alcohol");
		
		ArrayList<String> issue = new ArrayList<String>();
		issue.add("Overweight");
		
		user.setHabits(habbits);
		user.setCurrentHealth(issue);
		
		HealthMainClass hmc = new HealthMainClass();
		float premium = (float) hmc.createInsurancePremium(user);
		
		System.out.println("Health Insurance Premium for Mr. Gomes Rs" +premium);

	}
	

}

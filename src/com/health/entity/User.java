package com.health.entity;

import java.util.ArrayList;

public class User {
	
	private String name;
	
	private int age;
	
	private String gender;
	
	private ArrayList<String> habits;
	
	private ArrayList<String> currentHealth;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public ArrayList<String> getHabits() {
		return habits;
	}

	public void setHabits(ArrayList<String> habits) {
		this.habits = habits;
	}

	public ArrayList<String> getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(ArrayList<String> currentHealth) {
		this.currentHealth = currentHealth;
	}
	
	

}
